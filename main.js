var waitingText = document.getElementById('waiting-text');
var results = document.getElementById('results');
var waitingLoader = document.getElementById('waiting-loader');

// initial state
results.innerHTML = '';

// create new IntersectionObserver to observe when the elt is shown into the viewport
const observer = new IntersectionObserver( entries => {

    entries.forEach( entry => {
        // if the element is actually visible on screen viewport
        if (entry.intersectionRatio > 0) {
            waitingLoader.style.display = 'block';
            
            // simulate iddle time while fetching data from server
            setTimeout( async () => {
                const res = await fetch('users.json');
                const users = await res.json();
    
                let output = '';
                users.forEach( user => {
                    output += `<div class="user">
                                    ${user.id}. ${user.name} from <span>${user.anime}</span>
                                </div>`;
                });
                // display results
                waitingLoader.style.display = 'none';
                results.innerHTML = output;
            }, 2000);
            

        } else {
            // reset
            waitingLoader.style.display = 'none';
            results.innerHTML = '';
        }
    })
})
// launch observer
observer.observe(waitingText);